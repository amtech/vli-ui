<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <!-- 避免IE使用兼容模式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>容器（container）</title>
    <script type="text/javascript" src="./../../../static/js/include/_include-css.js"></script>
</head>


<body class="bg-gray">
    <div class="wrapper">
        <div class="wrapper-container">
        </div>
    </div>
    <script type="text/javascript" src="./../../../static/js/include/_include-js.js"></script>
</body>

</html>