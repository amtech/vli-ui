# VLI-UI

#### 介绍
构建基于bootstrap3.4.1改造的后台管理UI，融合一些常用的插件和样式。方便与后台开发时直接应用。

[页面预览]( http://leeatao.gitee.io/vli-ui)

#### 软件架构
参考应用的组件有：

bootstrap3.4.1

jQuery blockUI

bootstrap-table

colpick

contextMenu

cropper

echarts
