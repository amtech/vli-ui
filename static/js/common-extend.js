/* 公用js扩展类 */
/* panel工具栏中，折叠/关闭按钮事件 */
jQuery('.panel .tools .panel-collapse')
    .click(
        function () {
            var el = jQuery(this).parents(".panel").children(
                ".panel-body");
            if (jQuery(this).hasClass("fa-chevron-down")) {
                jQuery(this).removeClass("fa-chevron-down")
                    .addClass("fa-chevron-up");
                el.slideUp(200);
            } else {
                jQuery(this).removeClass("fa-chevron-up").addClass(
                    "fa-chevron-down");
                el.slideDown(200);
            }
        });
jQuery('.panel .tools .fa-times').click(function () {
    jQuery(this).parents(".panel").parent().remove();
});

$("[data-toggle='tooltip']").tooltip();//消息提示初始化方法
$('[data-toggle="popover"]').popover();//弹出框初始化方法

$(function () {
    loadTheme();
    //  layer扩展皮肤
    if (window.layer !== undefined) {
        layer.config({
            skin: 'layer-reset'
        });
    }
    // select2复选框事件绑定
    if ($.fn.select2 !== undefined) {
        $.fn.select2.defaults.set("theme", "bootstrap");
        $("select.select2").each(function () {
            $(this).select2().on("change", function () {
                if ($.fn.validate && $(this).hasClass("required")) {
                    $(this).valid();
                }
            })
        })
    }
    //格式化输入插件绑定
    if ($.fn.formatter !== undefined) {
        $("input.formatter").each(function () {
            if ($(this).data("pattern") !== '') {
                $(this).formatter({
                    'pattern': $(this).data("pattern"),
                    'persistent': true
                });
            }
        })
    }

    if ($(".time-input").length > 0) {
        loadLayDate($(".time-input"));
    }

    if ($.fn.tagsInput != undefined) {
        $(".tags-input").each(function () {
            var height = $(this).data("height") ? $(this).data("height") : '36px';
            var text = $(this).attr("placeholder");
            $(this).tagsInput({
                height: height,
                width: '100%',
                defaultText: text,
                removeWithBackspace: true,
                delimiter: [',']
            });
        });
    }

});

function loadLayDate(elements) {
    $.each(elements, function (index, item) {
        var time = $(item);
        // 控制控件外观
        var type = time.attr("data-type") || 'date';
        // 控制回显格式
        var format = time.attr("data-format") || 'yyyy-MM-dd';
        // 控制日期控件按钮
        var buttons = time.attr("data-btn") || 'clear|now|confirm', newBtnArr = [];
        // 日期控件选择完成后回调处理
        var callback = time.attr("data-callback") || {};
        if (buttons) {
            if (buttons.indexOf("|") > 0) {
                var btnArr = buttons.split("|"), btnLen = btnArr.length;
                for (var j = 0; j < btnLen; j++) {
                    if ("clear" === btnArr[j] || "now" === btnArr[j] || "confirm" === btnArr[j]) {
                        newBtnArr.push(btnArr[j]);
                    }
                }
            } else {
                if ("clear" === buttons || "now" === buttons || "confirm" === buttons) {
                    newBtnArr.push(buttons);
                }
            }
        } else {
            newBtnArr = ['clear', 'now', 'confirm'];
        }
        laydate.render({
            elem: item,
            theme: 'reset', //自定义主题
            trigger: 'click',
            type: type,
            format: format,
            btns: newBtnArr,
            done: function (value, data) {
                try {
                    time.val(value);
                    $(time).valid();
                } catch (e) {
                }
                if (typeof window[callback] != 'undefined'
                    && window[callback] instanceof Function) {
                    window[callback](value, data);
                }
            }
        });
    });
}

function resetBodyThemeClass(className, iframes) {
    if ($.common.isNotEmpty(className) && iframes && iframes.length > 0) {
        $.each(iframes, function (i, child) {
            var childBody = $(child).contents().find('body');
            childBody.removeClass(function (index, css) {
                return (css.match(/(^|\s)theme-\S+/g) || []).join(' ');
            }).addClass(className);
            if (childBody.find('iframe') && childBody.find('iframe').length > 0) {
                resetBodyThemeClass(className, childBody.find('iframe'));
            }
        });
    }
}

function loadTheme() {
    /* 
    var themeClass = storage.get('theme-class');
    var topFrame = window.top;
    if (topFrame && $.common.isNotEmpty(themeClass)) {
        topFrame.$("body").removeClass(function (index, css) {
            return (css.match(/(^|\s)theme-\S+/g) || []).join(' ');
        }).addClass(themeClass);

        var childFrames = topFrame.$('body').find('iframe');
        if (childFrames.length > 0) {
            resetBodyThemeClass(themeClass, childFrames);
        }
    }
    var menuClass = storage.get('menu-class');
    if (topFrame && $.common.isNotEmpty(menuClass)) {
        topFrame.$("body").removeClass(function (index, css) {
            return (css.match(/(^|\s)menu-\S+/g) || []).join(' ');
        }).addClass(menuClass);
    } */
}
