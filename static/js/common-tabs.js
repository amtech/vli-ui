/* 公用任务栏处理类 */
//计算元素集合的总宽度
function calSumWidth(elements) {
    var width = 0;
    $(elements).each(function () {
        width += $(this).outerWidth(true);
    });
    return width;
}

// 激活指定选项卡
function setActiveTab(element) {
    if (!$(element).hasClass('active')) {
        var currentId = $(element).data('id');
        // 显示tab对应的内容区
        $('.index_iframe').each(function () {
            if ($(this).data('id') == currentId) {
                // $(this).show().siblings('.index_iframe').hide();
                hideIframe(this);
            }
        });
        $(element).addClass('active').siblings('.menuTab').removeClass('active');
        scrollToTab(element);
    }
}

//滚动到指定选项卡
function scrollToTab(element) {
    var marginLeftVal = calSumWidth($(element).prevAll()),
        marginRightVal = calSumWidth($(element).nextAll());
    // 可视区域非tab宽度
    var tabOuterWidth = calSumWidth($(".content-tabs").children().not(".menuTabs"));
    //可视区域tab宽度
    var visibleWidth = $(".content-tabs").outerWidth(true) - tabOuterWidth;
    //实际滚动宽度
    var scrollVal = 0;
    if ($(".page-tabs-content").outerWidth() < visibleWidth) {
        scrollVal = 0;
    } else if (marginRightVal <= (visibleWidth - $(element).outerWidth(true) - $(element).next().outerWidth(true))) {
        if ((visibleWidth - $(element).next().outerWidth(true)) > marginRightVal) {
            scrollVal = marginLeftVal;
            var tabElement = element;
            while ((scrollVal - $(tabElement).outerWidth()) > ($(".page-tabs-content").outerWidth() - visibleWidth)) {
                scrollVal -= $(tabElement).prev().outerWidth();
                tabElement = $(tabElement).prev();
            }
        }
    } else if (marginLeftVal > (visibleWidth - $(element).outerWidth(true) - $(element).prev().outerWidth(true))) {
        scrollVal = marginLeftVal - $(element).prev().outerWidth(true);
    }
    $('.page-tabs-content').animate({
        marginLeft: 0 - scrollVal + 'px'
    },
        "fast");
}

//查看左侧隐藏的选项卡
function scrollTabLeft() {
    var marginLeftVal = Math.abs(parseInt($('.page-tabs-content').css('margin-left')));
    // 可视区域非tab宽度
    var tabOuterWidth = calSumWidth($(".content-tabs").children().not(".menuTabs"));
    //可视区域tab宽度
    var visibleWidth = $(".content-tabs").outerWidth(true) - tabOuterWidth;
    //实际滚动宽度
    var scrollVal = 0;
    if (($(".page-tabs-content").width()) < visibleWidth) {
        return false;
    } else {
        var tabElement = $(".menuTab:first");
        var offsetVal = 0;
        while ((offsetVal + $(tabElement).outerWidth(true)) <= marginLeftVal) { //找到离当前tab最近的元素
            offsetVal += $(tabElement).outerWidth(true);
            tabElement = $(tabElement).next();
        }
        offsetVal = 0;
        if (calSumWidth($(tabElement).prevAll()) > visibleWidth) {
            while ((offsetVal + $(tabElement).outerWidth(true)) < (visibleWidth) && tabElement.length > 0) {
                offsetVal += $(tabElement).outerWidth(true);
                tabElement = $(tabElement).prev();
            }
            scrollVal = calSumWidth($(tabElement).prevAll());
        }
    }
    $('.page-tabs-content').animate({
        marginLeft: 0 - scrollVal + 'px'
    },
        "fast");
}

//查看右侧隐藏的选项卡
function scrollTabRight() {
    var marginLeftVal = Math.abs(parseInt($('.page-tabs-content').css('margin-left')));
    // 可视区域非tab宽度
    var tabOuterWidth = calSumWidth($(".content-tabs").children().not(".menuTabs"));
    //可视区域tab宽度
    var visibleWidth = $(".content-tabs").outerWidth(true) - tabOuterWidth;
    //实际滚动宽度
    var scrollVal = 0;
    if ($(".page-tabs-content").width() < visibleWidth) {
        return false;
    } else {
        var tabElement = $(".menuTab:first");
        var offsetVal = 0;
        while ((offsetVal + $(tabElement).outerWidth(true)) <= marginLeftVal) { //找到离当前tab最近的元素
            offsetVal += $(tabElement).outerWidth(true);
            tabElement = $(tabElement).next();
        }
        offsetVal = 0;
        while ((offsetVal + $(tabElement).outerWidth(true)) < (visibleWidth) && tabElement.length > 0) {
            offsetVal += $(tabElement).outerWidth(true);
            tabElement = $(tabElement).next();
        }
        scrollVal = calSumWidth($(tabElement).prevAll());
        if (scrollVal > 0) {
            $('.page-tabs-content').animate({
                marginLeft: 0 - scrollVal + 'px'
            },
                "fast");
        }
    }
}

function menuItem() {
    // 获取标识数据
    var dataUrl = $(this).attr('href'),
        dataIndex = $(this).data('index'),
        menuName = $.trim($(this).data('tab-title'));
    if (!menuName || $.common.isEmpty(menuName)) {
        menuName = $.trim($(this).text());
    }
    flag = true;
    $(".menubar ul li, .menubar li").removeClass("selected");
    $(this).parent("li").addClass("selected");
    setIframeUrl($(this).attr("href"));
    if (dataUrl == undefined || $.trim(dataUrl).length == 0) return false;

    // 选项卡菜单已存在
    $('.menuTab').each(function () {
        if ($(this).data('id') == dataUrl) {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active').siblings('.menuTab').removeClass('active');
                scrollToTab(this);
                // 显示tab对应的内容区
                $('.content-main .index_iframe').each(function () {
                    if ($(this).data('id') == dataUrl) {
                        // $(this).show().siblings('.index_iframe').hide();
                        hideIframe(this);
                        return false;
                    }
                });
            }
            flag = false;
            return false;
        }
    });
    // 选项卡菜单不存在
    if (flag) {
        var str = '<a href="javascript:;" class="active menuTab" data-id="' + dataUrl + '">' + menuName + ' <i class="fa fa-close-thin"></i></a>';
        $('.menuTab').removeClass('active');
        // 添加选项卡对应的iframe
        var str1 = '<iframe class="index_iframe" name="iframe' + dataIndex + '" width="100%" height="100%" src="' + dataUrl + '" frameborder="0" data-id="' + dataUrl + '" seamless></iframe>';
        $('.content-main').find('iframe.index_iframe').css("visibility", "hidden").parents('.content-main').append(str1);

        $.modal.loading("数据加载中，请稍后...");

        $('.content-main iframe:visible').on('load',function () {
            $.modal.closeLoading();
        });

        // 添加选项卡
        $('.menuTabs .page-tabs-content').append(str);
        scrollToTab($('.menuTab.active'));
    }
    return false;
}

function menuBlank() {
    // 新窗口打开外网以http://开头，
    var dataUrl = $(this).attr('href');
    window.open(dataUrl);
    return false;
}

// 关闭选项卡菜单
function closeTab() {
    var closeTabId = $(this).parents('.menuTab').data('id');
    var currentWidth = $(this).parents('.menuTab').width();
    var panelUrl = $(this).parents('.menuTab').data('panel');
    // 当前元素处于活动状态
    if ($(this).parents('.menuTab').hasClass('active')) {

        // 当前元素后面有同辈元素，使后面的一个元素处于活动状态
        if ($(this).parents('.menuTab').next('.menuTab').length) {

            var activeId = $(this).parents('.menuTab').next('.menuTab:eq(0)').data('id');
            $(this).parents('.menuTab').next('.menuTab:eq(0)').addClass('active');

            $('.content-main .index_iframe').each(function () {
                if ($(this).data('id') == activeId) {
                    // $(this).show().siblings('.index_iframe').hide();
                    hideIframe(this);
                    return false;
                }
            });

            var marginLeftVal = parseInt($('.page-tabs-content').css('margin-left'));
            if (marginLeftVal < 0) {
                $('.page-tabs-content').animate({
                    marginLeft: (marginLeftVal + currentWidth) + 'px'
                },
                    "fast");
            }

            //  移除当前选项卡
            $(this).parents('.menuTab').remove();

            // 移除tab对应的内容区
            $('.content-main .index_iframe').each(function () {
                if ($(this).data('id') == closeTabId) {
                    $(this).remove();
                    return false;
                }
            });
        }

        // 当前元素后面没有同辈元素，使当前元素的上一个元素处于活动状态
        if ($(this).parents('.menuTab').prev('.menuTab').length) {
            var activeId = $(this).parents('.menuTab').prev('.menuTab:last').data('id');
            $(this).parents('.menuTab').prev('.menuTab:last').addClass('active');
            $('.content-main .index_iframe').each(function () {
                if ($(this).data('id') == activeId) {
                    // $(this).show().siblings('.index_iframe').hide();
                    hideIframe(this);
                    return false;
                }
            });

            //  移除当前选项卡
            $(this).parents('.menuTab').remove();

            // 移除tab对应的内容区
            $('.content-main .index_iframe').each(function () {
                if ($(this).data('id') == closeTabId) {
                    $(this).remove();
                    return false;
                }
            });

            if ($.common.isNotEmpty(panelUrl)) {
                $('.menuTab[data-id="' + panelUrl + '"]').addClass('active').siblings('.menuTab').removeClass('active');
                $('.content-main .index_iframe').each(function () {
                    if ($(this).data('id') == panelUrl) {
                        // $(this).show().siblings('.index_iframe').hide();
                        hideIframe(this);
                        return false;
                    }
                });
            }
        }
    }
    // 当前元素不处于活动状态
    else {
        //  移除当前选项卡
        $(this).parents('.menuTab').remove();

        // 移除相应tab对应的内容区
        $('.content-main .index_iframe').each(function () {
            if ($(this).data('id') == closeTabId) {
                $(this).remove();
                return false;
            }
        });
    }
    scrollToTab($('.menuTab.active'));
    return false;
}

//滚动到已激活的选项卡
function showActiveTab() {
    scrollToTab($('.menuTab.active'));
}

// 点击选项卡菜单
function activeTab() {
    if (!$(this).hasClass('active')) {
        var currentId = $(this).data('id');
        // 显示tab对应的内容区
        $('.content-main .index_iframe').each(function () {
            if ($(this).data('id') == currentId) {
                // $(this).show().siblings('.index_iframe').hide();
                hideIframe(this);
                return false;
            }
        });
        $(this).addClass('active').siblings('.menuTab').removeClass('active');
        scrollToTab(this);
    }
}

// 刷新iframe
function refreshTab() {
    var currentId = $('.page-tabs-content').find('.active').attr('data-id');
    var target = $('.index_iframe[data-id="' + currentId + '"]');
    var url = target.attr('src');
    target.attr('src', url).ready();
}

// 页签全屏
function fullScreenTab() {
    var currentId = $('.page-tabs-content').find('.active').attr('data-id');
    var target = $('.index_iframe[data-id="' + currentId + '"]');
    target.fullScreen(true);
}

// 关闭当前选项卡
function tabCloseCurrent() {
    $('.page-tabs-content').find('.active i').trigger("click");
}

//关闭其他选项卡
function tabCloseOther() {
    $('.page-tabs-content').children("[data-id]").not(":first").not(".active").each(function () {
        $('.index_iframe[data-id="' + $(this).data('id') + '"]').remove();
        $(this).remove();
    });
    $('.page-tabs-content').css("margin-left", "0");
}

// 关闭全部选项卡
function tabCloseAll() {
    $('.page-tabs-content').children("[data-id]").not(":first").each(function () {
        $('.index_iframe[data-id="' + $(this).data('id') + '"]').remove();
        $(this).remove();
    });
    $('.page-tabs-content').children("[data-id]:first").each(function () {
        $('.index_iframe[data-id="' + $(this).data('id') + '"]').css("visibility", "visible");
        $(this).addClass("active");
    });
    $('.page-tabs-content').css("margin-left", "0");
}

function hideIframe(iframe) {
    $(iframe).css("visibility", "visible");
    $(iframe).siblings('.index_iframe').each(function () {
        $(this).css("visibility", "hidden");
    });
}

/** 创建选项卡 */
function createMenuItem(dataUrl, menuName) {
    var panelUrl = window.frameElement.getAttribute('data-id');
    dataIndex = $.common.random(1, 100),
        flag = true;
    if (dataUrl == undefined || $.trim(dataUrl).length == 0) return false;
    var topWindow = $(window.parent.document);
    // 选项卡菜单已存在
    $('.menuTab', topWindow).each(function () {
        if ($(this).data('id') == dataUrl) {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active').siblings('.menuTab').removeClass('active');
                $('.page-tabs-content').animate({marginLeft: ""}, "fast");
                // 显示tab对应的内容区
                $('.content-main .index_iframe', topWindow).each(function () {
                    if ($(this).data('id') == dataUrl) {
                        // $(this).show().siblings('.index_iframe').hide();
                        hideIframe(this);
                        return false;
                    }
                });
            }
            flag = false;
            return false;
        }
    });
    // 选项卡菜单不存在
    if (flag) {
        var str = '<a href="javascript:;" class="active menuTab" data-id="' + dataUrl + '" data-panel="' + panelUrl + '">' + menuName + ' <i class="fa fa-close-thin"></i></a>';
        $('.menuTab', topWindow).removeClass('active');

        // 添加选项卡对应的iframe
        var str1 = '<iframe class="index_iframe" name="iframe' + dataIndex + '" width="100%" height="100%" src="' + dataUrl + '" frameborder="0" data-id="' + dataUrl + '" data-panel="' + panelUrl + '" seamless></iframe>';
        $('.content-main', topWindow).find('iframe.index_iframe').css("visibility", "hidden").parents('.content-main').append(str1);

        window.parent.$.modal.loading("数据加载中，请稍后...");
        $('.content-main iframe:visible', topWindow).on('load',function () {
            window.parent.$.modal.closeLoading();
        });

        // 添加选项卡
        $('.menuTabs .page-tabs-content', topWindow).append(str);
    }
    return false;
}


/** 刷新选项卡 */
var refreshItem = function () {
    var topWindow = $(window.parent.document);
    var currentId = $('.page-tabs-content', topWindow).find('.active').attr('data-id');
    var target = $('.index_iframe[data-id="' + currentId + '"]', topWindow);
    var url = target.attr('src');
    target.attr('src', url).ready();
}

/** 关闭选项卡 */
var closeItem = function (dataId) {
    var topWindow = $(window.parent.document);
    if ($.common.isNotEmpty(dataId)) {
        window.parent.$.modal.closeLoading();
        // 根据dataId关闭指定选项卡
        $('.menuTab[data-id="' + dataId + '"]', topWindow).remove();
        // 移除相应tab对应的内容区
        $('.content-main .index_iframe[data-id="' + dataId + '"]', topWindow).remove();
        return;
    }
    var panelUrl = window.frameElement.getAttribute('data-panel');
    $('.page-tabs-content .active i', topWindow).click();
    if ($.common.isNotEmpty(panelUrl)) {
        $('.menuTab[data-id="' + panelUrl + '"]', topWindow).addClass('active').siblings('.menuTab').removeClass('active');
        $('.content-main .index_iframe', topWindow).each(function () {
            if ($(this).data('id') == panelUrl) {
                // $(this).show().siblings('.index_iframe').hide();
                hideIframe(this);
                return false;
            }
        });
    }
}



/* No userd */

mode = 'history';//设置默认方式
// 设置锚点
function setIframeUrl(href) {
    if ($.common.equals("history", mode)) {
        storage.set('publicPath', href);
    } else {
        var nowUrl = window.location.href;
        var newUrl = nowUrl.substring(0, nowUrl.indexOf("#"));
        window.location.href = newUrl + "#" + href;
    }
}


// 双击选项卡全屏显示
function activeTabMax() {
    $('#content-main').toggleClass('max');
    $('#ax_close_max').show();
}
