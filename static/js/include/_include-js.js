document.writeln('<script src="./../../../static/js/jquery.min.js"></script>');
document.writeln('<script src="./../../../static/js/bootstrap.js"></script>');
/* select2插件 */
document.writeln('<script src="./../../../static/plugins/select2/select2.js"></script>');
/* 表单验证插件 */
document.writeln('<script src="./../../../static/plugins/validate/jquery.validate.js"></script>');
document.writeln('<script src="./../../../static/plugins/validate/jquery.validate.extend.js"></script>');
document.writeln('<script src="./../../../static/plugins/validate/messages_zh.min.js"></script>');
/* 日期时间插件 */
document.writeln('<script src="./../../../static/plugins/laydate/laydate.js"></script>');
/* layer弹窗插件 */
document.writeln('<script src="./../../../static/plugins/layer/layer.min.js"></script>');
/* 弹窗遮罩插件 */
document.writeln('<script src="./../../../static/plugins/blockUI/jquery.blockUI.js"></script>');
/* bootstrap-table插件 */
document.writeln('<script src="./../../../static/plugins/bootstrap-table/bootstrap-table.min.js"></script>');
document.writeln('<script src="./../../../static/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>');
/* bootstrap-treetable插件 */
document.writeln('<script src="./../../../static/plugins/bootstrap-treetable/bootstrap-treetable.js"></script>');
/* layout布局插件 */
document.writeln('<script src="./../../../static/plugins/jquery-layout/jquery.layout-latest.js"></script>');
/* 标签输入插件 */
document.writeln('<script type="text/javascript" src="./../../../static/plugins/tagsinput/jquery.tagsinput.js"></script>');
document.writeln('<script type="text/javascript" src="./../../../static/plugins/jquery-ui/jquery-ui.js"></script>');

/* 全局公用 */
document.writeln('<script src="./../../../static/js/common-core.js"></script>');
document.writeln('<script src="./../../../static/js/common-tabs.js"></script>');
document.writeln('<script src="./../../../static/js/common-extend.js"></script>');