document.writeln('<link href="./../../../static/css/bootstrap.css" rel="stylesheet" />');
document.writeln('<link href="./../../../static/css/font-awesome.min.css" rel="stylesheet" />');
document.writeln('<link href="./../../../static/css/animate.css" rel="stylesheet" />');
document.writeln('<link href="./../../../static/plugins/jquery-ui/jquery-ui.css" rel="stylesheet" />');
/* 插件 */
document.writeln('<link href="./../../../static/plugins/select2/select2.css" rel="stylesheet" />');
document.writeln('<link href="./../../../static/plugins/select2/select2-bootstrap.css" rel="stylesheet" />');

/* bootstrap-table插件 */
document.writeln('<link href="./../../../static/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" />');
/* layout布局插件 */
document.writeln('<link href="./../../../static/plugins/jquery-layout/jquery.layout-latest.css" rel="stylesheet" />');
/* 标签输入插件 */
document.writeln('<link href="./../../../static/plugins/tagsinput/jquery.tagsinput.css" rel="stylesheet" />');

/* 自定义样式 */
document.writeln('<link href="./../../../static/css/styles.css" rel="stylesheet" />');
document.writeln('<link href="./../../../static/css/theme-all.css" rel="stylesheet" />');
