$(function () {
    $("[id^='side-menu']").metisMenu();
    bindMiniMenuHover();
    // 菜单切换
    $('.mini-switch-bar').click(function () {
        $("body").toggleClass("mini-menu");
        bindMiniMenuHover();
    });

    $('.sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0,
        alwaysVisible: true
    });


    //通过遍历给菜单项加上data-index属性
    $(".menuItem").each(function (index) {
        if (!$(this).attr('data-index')) {
            $(this).attr('data-index', index);
        }
    });

    $('.menuItem').on('click', menuItem);

    $('.menuBlank').on('click', menuBlank);

    $('.menuTabs').on('click', '.menuTab i', closeTab);

    $('.tabShowActive').on('click', showActiveTab);



    // 点击选项卡菜单
    $('.menuTabs').on('click', '.menuTab', activeTab);

    // 全屏显示
    $('#fullScreen').on('click', function () {
        $(document).toggleFullScreen();
    });

    // 页签刷新按钮
    $('.tabReload').on('click', refreshTab);

    // 页签全屏按钮
    $('.tabFullScreen').on('click', fullScreenTab);

    // 双击选项卡全屏显示
    // $('.menuTabs').on('dblclick', '.menuTab', activeTabMax);

    // 左移按扭
    $('.tabLeft').on('click', scrollTabLeft);

    // 右移按扭
    $('.tabRight').on('click', scrollTabRight);

    // 关闭当前
    $('.tabCloseCurrent').on('click', tabCloseCurrent);

    // 关闭其他
    $('.tabCloseOther').on('click', tabCloseOther);

    // 关闭全部
    $('.tabCloseAll').on('click', tabCloseAll);

    // tab全屏显示
    $('.tabMaxCurrent').on('click', function () {
        $('.page-tabs-content').find('.active').trigger("dblclick");
    });

    // 关闭全屏
    $('#ax_close_max').click(function () {
        $('#content-main').toggleClass('max');
        $('#ax_close_max').hide();
    })


    $(window).keydown(function (event) {
        if (event.keyCode == 27) {
            $('#content-main').removeClass('max');
            $('#ax_close_max').hide();
        }
    });

    window.onhashchange = function () {
        var hash = location.hash;
        var url = hash.substring(1, hash.length);
        $('a[href$="' + url + '"]').click();
    };
    // // 右键菜单实现
    $.contextMenu({
        selector: ".menuTab",
        trigger: 'right',
        autoHide: true,
        items: {
            "close_current": {
                name: "关闭当前",
                icon: "fa-close",
                callback: function (key, opt) {
                    opt.$trigger.find('i').trigger("click");
                }
            },
            "close_other": {
                name: "关闭其他",
                icon: "fa-window-close-o",
                callback: function (key, opt) {
                    setActiveTab(this);
                    tabCloseOther();
                }
            },
            "close_left": {
                name: "关闭左侧",
                icon: "fa-reply",
                callback: function (key, opt) {
                    setActiveTab(this);
                    this.prevAll('.menuTab').not(":last").each(function () {
                        if ($(this).hasClass('active')) {
                            setActiveTab(this);
                        }
                        $('.index_iframe[data-id="' + $(this).data('id') + '"]').remove();
                        $(this).remove();
                    });
                    $('.page-tabs-content').css("margin-left", "0");
                }
            },
            "close_right": {
                name: "关闭右侧",
                icon: "fa-share",
                callback: function (key, opt) {
                    setActiveTab(this);
                    this.nextAll('.menuTab').each(function () {
                        $('.menuTab[data-id="' + $(this).data('id') + '"]').remove();
                        $(this).remove();
                    });
                }
            },
            "close_all": {
                name: "全部关闭",
                icon: "fa-window-close",
                callback: function (key, opt) {
                    tabCloseAll();
                }
            },
            "step": "---------",
            "full": {
                name: "全屏显示",
                icon: "fa-arrows-alt",
                callback: function (key, opt) {
                    setActiveTab(this);
                    var target = $('.index_iframe[data-id="' + this.data('id') + '"]');
                    target.fullScreen(true);
                }
            },
            "refresh": {
                name: "刷新页面",
                icon: "fa-refresh",
                callback: function (key, opt) {
                    setActiveTab(this);
                    var target = $('.index_iframe[data-id="' + this.data('id') + '"]');
                    var url = target.attr('src');
                    $.modal.loading("数据加载中，请稍后...");
                    target.attr('src', url).on('load', function () {
                        $.modal.closeLoading();
                    });
                }
            },
            "open": {
                name: "新窗口打开",
                icon: "fa-link",
                callback: function (key, opt) {
                    var target = $('.index_iframe[data-id="' + this.data('id') + '"]');
                    window.open(target.attr('src'));
                }
            },
        }
    });
});


$(window).bind("load resize",
    function () {
        if ($(this).width() < 769) {
            $('body').addClass('mini-menu');
            bindMiniMenuHover();
        } else {
            $('body').removeClass('mini-menu');
        }
    });

//绑定mini菜单hover事件处理
function bindMiniMenuHover() {
    if ($("body").hasClass("mini-menu")) {
        //处理mini菜单滚动条和显示问题
        $(".mini-menu .main-left #side-menu > li ").hover(function () {
            $(this).parents(".sidebar-collapse").addClass("hovered");
            $(this).addClass("hovered");
        }, function () {
            $(this).parents(".sidebar-collapse").removeClass("hovered");
            $(this).removeClass("hovered");
        });
    }
}