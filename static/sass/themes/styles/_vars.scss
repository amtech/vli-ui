$base-primary: $common-base-primary !default;
$base-success: $common-base-success !default;
$base-info: $common-base-info !default;
$base-warning: $common-base-warning !default;
$base-danger: $common-base-danger !default;

$white-color:$white-color !default; //白色
$base-gray-color:#f5f5f5 !default;
$base-border-color:#eee !default;

// body样式
$body-bg-color:#f5f5f5 !default; //body背景颜色
$body-text-color: $common-text-color !default; //body文字颜色
// 系统滚动条样式
$scrollbar-bg-color:#f5f5f5 !default; //滚动条背景
$scrollbar-color:#999 !default; //自定义滚动条颜色

// 布局颜色变量
$frame-head-bgcolor:$base-primary !default; //顶部菜单背景色
$frame-head-logo-bgcolor:darken($base-primary, 5%) !default; //顶部LOGO区域背景色
$frame-head-logo-color:$white-color !default; //顶部LOGO文字颜色
$frame-head-color:$white-color !default; //顶部菜单文字颜色

$frame-left-bg:#222D39 !default; //左侧菜单默认背景颜色
$frame-left-color:#a7b1c2 !default; //菜单默认文字颜色
$frame-left-hover-color:$white-color !default; //菜单默认文字激活颜色
$frame-left-select-bg:darken($base-primary, 5%) !default; //激活菜单背景
$frame-left-select-color:$white-color !default; //激活菜单文字颜色
$frame-left-select-bdcolor:$frame-left-select-bg !default; //激活菜单边框
$frame-left-active-bg:darken($frame-left-bg, 5%) !default; //激活菜单背景
$frame-left-active-color:$white-color !default; //激活菜单背景
$frame-left-mini-border-color:$frame-left-bg !default;

$frame-content-tab-bgcolor:$white-color !default; //任务栏背景
$frame-content-tab-bdcolor:$base-border-color !default; //任务栏边框颜色
$frame-content-tab-rollbar-color:#999 !default; //任务栏操作按钮文字颜色
$frame-content-tab-rollbar-hover-color:#fafafa !default; //任务栏操作按钮激活颜色
$frame-content-tab-rollbar-bdcolor:$base-border-color !default; //任务栏操作按钮边框颜色
$frame-content-tab-pagebar-color:#999 !default; //任务栏页面按钮文字颜色
$frame-content-tab-pagebar-icon-color:#999 !default; //任务栏页面按钮文字颜色

$frame-content-tab-pagebar-bdcolor:$base-border-color !default; //任务栏页面按钮文字颜色
$frame-content-tab-pagebar-hover-bgcolor:mix($base-primary, $white-color, 5%) !default; //任务栏页面按钮文字颜色
$frame-content-tab-pagebar-active-bgcolor:mix($base-primary, $white-color, 5%) !default; //任务栏页面按钮文字颜色
$frame-content-tab-pagebar-active-color:$base-primary !default;

// 分割线样式
$hr-border-color:$base-border-color !default;
$hr-color:$white-color !default;
$hr-bg-color:$white-color !default;

// bootstrap-table样式定义
$table-thead-bg-color:$base-gray-color !default; //表头背景
$table-striped-odd-bg:$white-color !default; //交替颜色奇数行背景
$table-striped-odd-even:#fafafa !default; //交替颜色偶数行背景
$table-tr-hover-bg:#fafafa !default; //表格行Hover背景颜色
$table-tr-selected-bg:mix($base-primary, $white-color, 5%) !default; //表格行选中颜色
$table-fix-columns-bg:$white-color !default; //冻结列背景色
$table-select-color:$base-primary !default; //选中行文字颜色
$table-border-color : $base-border-color !default; //表格边框颜色
$table-drag-rows-color:$white-color !default; //行拖动文字颜色
$table-drag-rows-bg:rgba($base-primary, 0.8) !default; //行拖动背景颜色
$table-drag-colunm-bg:$white-color !default; //列拖动背景
$table-drag-colunm-shadow-color:darken($base-border-color, 10%) !default; //列拖动背景

// bootstrap-treetable
$treetable-thead-bg:$base-gray-color !default; //表头背景颜色
$treetable-tr-hover-bg:#fafafa !default; //行Hover背景
$treetable-border-color:$base-border-color !default;

// timeline
$timeline-simple-bg:$base-gray-color !default;
$timeline-simple-border:$base-border-color !default;
$timeline-bg:$white-color !default;
$timeline-line-color:darken($base-gray-color, 10%) !default;
$timeline-icon-default-bg:darken($base-gray-color, 10%) !default;
$timeline-icon-default-color:$body-text-color !default;
$timeline-icon-primary-bg:$base-primary !default;
$timeline-icon-primary-color:$white-color !default;
$timeline-icon-success-bg:$base-success !default;
$timeline-icon-success-color:$white-color !default;
$timeline-icon-info-bg:$base-info !default;
$timeline-icon-info-color:$white-color !default;
$timeline-icon-warning-bg:$base-warning;
$timeline-icon-warning-color:$white-color !default;
$timeline-icon-danger-bg:$base-danger !default;
$timeline-icon-danger-color:$white-color !default;
$timeline-item-bg:$base-gray-color !default;
$timeline-item-border:$base-gray-color !default;

// swiper轮播图
$swiper-icon-color: rgba($color: $white-color, $alpha: 0.5) !default;
$swiper-icon-active-color: rgba($color: $white-color, $alpha: 0.8) !default;
$swiper-icon-bg:rgba(0, 0, 0, 0.2) !default;

// 组件重置样式
$select2-border-color:$base-primary !default; //select2边框颜色
$select2-selected-bg:$base-primary !default; //select2选中背景色
$select2-result-color:#ccc !default; //select2选中背景色

// slider颜色
$slider-bg:$base-primary !default;

// search-panel
$search-panel-bg:$white-color !default;
$search-panel-shadow:1px 1px 3px rgba(#000000, 0.2) !default;
$search-panel-list-item-color:$body-text-color !default;
$search-panel-list-item-focus-border:$base-primary !default;
$search-panel-list-item-border:$base-border-color !default;

$checkbox-border:darken($base-border-color, 35%) !default;
// checkbox
$checkbox-border:$checkbox-border !default;
$checkbox-checked-border:$base-primary !default;
$checkbox-checked-bg:$white-color !default;

// radio
$radio-border:darken($base-border-color, 40%) !default;
$radio-checked-border:$base-primary !default;

// toggle-switch
$toggle-switch-border:$base-border-color !default;
$toggle-switch-bg:$white-color !default;

$toggle-switch-color-default:$base-primary !default;

$toggle-switch-color-primary:$base-primary !default;
$toggle-switch-color-success:$base-success !default;
$toggle-switch-color-info:$base-info !default;
$toggle-switch-color-warning:$base-warning !default;
$toggle-switch-color-danger:$base-danger !default;

// switch-group
$switch-group-title-border:$base-border-color !default;
$switch-group-title-bg:$base-gray-color !default;
$switch-group-checked-border:$base-primary !default;
$switch-group-checked-bg:$switch-group-checked-border !default;
$switch-group-checked-color:$white-color !default;

$switch-radio-primary-bg:$base-primary !default;
$switch-radio-primary-border:$switch-radio-primary-bg !default;

$switch-radio-success-bg:$base-success !default;
$switch-radio-success-border:$switch-radio-success-bg !default;
$switch-radio-info-bg:$base-info !default;
$switch-radio-info-border:$switch-radio-info-bg !default;
$switch-radio-warning-bg:$base-warning !default;
$switch-radio-warning-border:$switch-radio-warning-bg !default;
$switch-radio-danger-bg:$base-danger !default;
$switch-radio-danger-border:$switch-radio-danger-bg !default;

// form相关细节
$input-error-border:$base-danger !default;
$required-fonts-color:$base-danger !default;
$form-header-border:$base-border-color !default;
$form-header-color:$base-primary !default;
$form-label-error-color:$base-danger !default;

// laydate
$laydate-main-color:$base-primary !default;
// layer
$layer-titile-bg:#f8f8f8 !default;
$layer-titile-color:$body-text-color !default;

$layer-btn-box-bg:mix($base-primary, $white-color, 6%) !default;
$layer-btn-box-border:$base-border-color !default;

$layer-btn-default-color:$white-color !default;
$layer-btn-default-bdcolor:$base-primary !default;
$layer-btn-default-bgcolor:$base-primary !default;

$layer-btn-first-color:darken($body-text-color, 15%) !default;
$layer-btn-first-bdcolor:darken($base-border-color, 15%) !default;
$layer-btn-first-bgcolor:$white-color !default;

$layer-btn-second-color:$white-color !default;
$layer-btn-second-bdcolor:$base-warning !default;
$layer-btn-second-bgcolor:$base-warning !default;

$layer-btn-third-color:$white-color !default;
$layer-btn-third-bdcolor:$base-danger !default;
$layer-btn-third-bgcolor:$base-danger !default;

// loaderbox遮罩提示
$loaderbox-color:#000 !default;
$loaderbox-border:$base-border-color !default;
$loaderbox-bgcolor:$base-border-color !default;


$form-input-focus-bdcolor:rgba(red($base-primary), green($base-primary), blue($base-primary), .6) !default;